# nitro-cli

This projects build nitro-cli into binary packages.

## Building docker image

```
$ make -C docker build
```

To fetch the image, run `docker pull registry.gitlab.com/blocksq/nitro-cli`.

## Building dpkg package

Docker is required.

```
$ git submodule update --init --recursive
$ make -C dpkg build
```

The dpkg is in the `build` directory.

If you are updating the nitro-cli source, remember to change the version name in
the Makefile as well as `DEBIAN/control` file.

There is a [kernel extension built for
Ubuntu](https://github.com/aws/aws-nitro-enclaves-cli/blob/main/docs/ubuntu_20.04_how_to_install_nitro_cli_from_github_sources.md)
but there is no kernel extension built for debian. Installing the package in
debian is not tested.
